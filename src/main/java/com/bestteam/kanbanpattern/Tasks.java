package com.bestteam.kanbanpattern;

public class Tasks {

  private static int counter = 0;
  private  int productNumber;

  public Tasks() {
    counter++;
    productNumber = counter;
  }

  @Override
  public String toString() {
    return "Task_" + productNumber;
  }
}
