package com.bestteam.kanbanpattern;

import com.bestteam.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class App {

    public static final Logger logger = LogManager.getLogger(App.class);
    private static final String slashN = "\n";

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Task task = new Task();
        do {
            logger.info(slashN + Color.RED_BOLD);
            logger.info("   (1 - Add Tasks)");
            logger.info(Color.MAGENTA_BOLD);
            logger.info(slashN + "   (2 - Make Tasks In Progress)");
            logger.info(Color.YELLOW_BOLD);
            logger.info(slashN + "   (3 - Review Task's Code)");
            logger.info(Color.GREEN_BOLD);
            logger.info(slashN + "   (4 - Test Task)");
            logger.info(Color.BLUE_BOLD);
            logger.info(slashN + "   (5 - Approve Task)");
            logger.info(Color.BLACK_BOLD);
            logger.info(slashN + "   ---------------------------------");
            logger.info(slashN + "   (L - Get Tasks List) : (Q - exit)");
            logger.info(slashN + "   ---------------------------------");
            logger.info(Color.CYAN_BOLD);
            logger.info(slashN + "   Please, select menu point:");
            logger.info(Color.RESET);
            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        task.addTask();
                        break;
                    case "2":
                        task.inProgress();
                        break;
                    case "3":
                        task.doCodeReview();
                        break;
                    case "4":
                        task.testCode();
                        break;
                    case "5":
                        task.approveTask();
                        break;
                    case "L":
                        task.getTaskList();
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
