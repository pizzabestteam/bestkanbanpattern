package com.bestteam.kanbanpattern.stateimpl;

import com.bestteam.kanbanpattern.State;
import com.bestteam.kanbanpattern.Task;

import static com.bestteam.kanbanpattern.App.logger;

public class ReviewState implements State {

  @Override
  public void addTask(Task task) {
    task.addTaskToList();
    task.setState(new NewTaskState());
    logger.info("Add one more task");
  }

  @Override
  public void testCode(Task task) {
    task.setState(new TestState());
    logger.info("Task is testing");
  }
}
