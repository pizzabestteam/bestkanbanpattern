package com.bestteam.kanbanpattern.stateimpl;

import com.bestteam.kanbanpattern.State;
import com.bestteam.kanbanpattern.Task;

import static com.bestteam.kanbanpattern.App.logger;

public class NewTaskState implements State {

  @Override
  public void addTask(Task task) {
    task.addTaskToList();
  }

  @Override
  public void inProgress(Task task) {
    task.setState(new ProgressState());
    logger.info("Task is in progress");
  }
}
