package com.bestteam.kanbanpattern.stateimpl;

import com.bestteam.kanbanpattern.Task;
import com.bestteam.kanbanpattern.State;

import static com.bestteam.kanbanpattern.App.logger;

public class ProgressState implements State {

  @Override
  public void addTask(Task task) {
    task.addTaskToList();
    task.setState(new NewTaskState());
    System.out.println("Add one more task");
  }

  @Override
  public void doCodeReview(Task task) {
    task.setState(new ReviewState());
    logger.info("Task's code is reviewing");
  }
}
