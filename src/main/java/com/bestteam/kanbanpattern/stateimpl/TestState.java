package com.bestteam.kanbanpattern.stateimpl;

import com.bestteam.kanbanpattern.Task;
import com.bestteam.kanbanpattern.State;

import static com.bestteam.kanbanpattern.App.logger;

public class TestState implements State {
  @Override
  public void approveTask(Task task) {
    task.setState(new ApprovingState());
    logger.info("Task is done and approved");
  }
}
