package com.bestteam.kanbanpattern;

import com.bestteam.Color;
import com.bestteam.kanbanpattern.stateimpl.NewTaskState;

import java.util.LinkedList;
import java.util.List;

import static com.bestteam.kanbanpattern.App.logger;

public class Task {

  private State state;
  private List<Tasks> tasks = new LinkedList<>();

  public Task() {
    state = new NewTaskState();
  }

  public void setState(State state) {
    this.state = state;
  }

  public void addTask() {
    state.addTask(this);
  }

  public void inProgress() {
    if (tasks.isEmpty()) {
      logger.info("Task is empty!");
    } else {
      state.inProgress(this);
    }
  }

  public void doCodeReview() {
    state.doCodeReview(this);
  }

  public void testCode() {
    state.testCode(this);
  }

  public void approveTask() {
    state.approveTask(this);
  }

  public void addTaskToList() {
    Tasks tasks = new Tasks();
    this.tasks.add(tasks);
    logger.info(Color.MAGENTA_UNDERLINED + "" + tasks + Color.RESET + " add to Bucket");
  }

  public void getTaskList() {
    logger.info(tasks);
  }
}
