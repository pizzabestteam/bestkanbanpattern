package com.bestteam.kanbanpattern;

import static com.bestteam.kanbanpattern.App.logger;

public interface State {


    default void addTask(Task task) {
        logger.info("addTask - is not allowed");
    }

    default void inProgress(Task task) {
        logger.info("inProgress - is not allowed");
    }

    default void doCodeReview(Task task) {
        logger.info("doCodeReview - is not allowed");
    }

    default void testCode(Task task) {
        logger.info("testCode - is not allowed");
    }

    default void approveTask(Task task) { logger.info("approveTask - is not allowed");
    }

    default void cancel(Task task) {
        logger.info("cancel - is not allowed");
    }
}
